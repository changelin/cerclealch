class Shape(object):
    def __init__(self):
        shapes = []

    def evolve(self):
        raise NotImplementedError( "Should have implemented this" )

    def draw(self):
        raise NotImplementedError( "Should have implemented this" )
